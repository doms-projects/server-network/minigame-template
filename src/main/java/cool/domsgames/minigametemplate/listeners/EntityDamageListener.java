package cool.domsgames.minigametemplate.listeners;

import cool.domsgames.minigametemplate.GameManager;
import cool.domsgames.minigametemplate.enums.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public record EntityDamageListener(GameManager gameManager) implements Listener {

    @EventHandler(priority = EventPriority.LOWEST) // Execute first
    public void entityDamage(EntityDamageEvent e) {
        if (gameManager.getGameState() != GameState.STARTED) e.setCancelled(true);
    }
}
