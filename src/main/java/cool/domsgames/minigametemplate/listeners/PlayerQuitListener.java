package cool.domsgames.minigametemplate.listeners;

import cool.domsgames.minigametemplate.GameManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public record PlayerQuitListener(GameManager gameManager) implements Listener {

    @EventHandler
    public void playerQuit(PlayerQuitEvent e) {
        gameManager.removePlayer(e.getPlayer());
    }
}
