package cool.domsgames.minigametemplate;

import cool.domsgames.minigametemplate.enums.GameState;
import cool.domsgames.minigametemplate.events.GameEndEvent;
import cool.domsgames.minigametemplate.events.GameRunningTimeEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;

public class GameManager {

    private final int GAME_COUNTDOWN_LENGTH_SECONDS = 60;
    private final HashMap<Integer, String> COUNTDOWN_TITLE_NUMBERS = new HashMap<>() {{
        put(60, "Starting in 60 seconds");
        put(30, "Starting in 30 seconds");
        put(20, "Starting in 20 seconds");
        put(10, "❿");
        put(9, "❾");
        put(8, "❽");
        put(7, "❼");
        put(6, "❻");
        put(5, "❺");
        put(4, "❹");
        put(3, ChatColor.GREEN + "❸");
        put(2, ChatColor.DARK_AQUA + "❷");
        put(1, ChatColor.BLUE + "❶");
    }};

    private final JavaPlugin plugin;
    private final GameManagerOptions options;

    private final HashSet<Player> inGamePlayers = new HashSet<>();
    private final HashSet<Player> spectatingPlayers = new HashSet<>();

    private GameState gameState = GameState.PREGAME;
    private final HashMap<GameState, Integer> repeatingTasks = new HashMap<>();

    public GameManager(JavaPlugin plugin, GameManagerOptions options) {
        this.plugin = plugin;
        this.options = options;

        startReminderChatMessages();
    }

    private void sendTitleToAllPlayers(String title) {
        plugin.getServer().getOnlinePlayers().forEach(player -> player.sendTitle(title, "", 0, 25, 5));
    }

    private boolean hasRepeatingTask(GameState gameState) {
        return repeatingTasks.containsKey(gameState);
    }

    private void setRepeatingTask(GameState gameState, int taskId) {
        cancelRepeatingTask(gameState);
        repeatingTasks.put(gameState, taskId);
    }

    private void cancelRepeatingTask(GameState gameState) {
        if (!repeatingTasks.containsKey(gameState)) return;
        plugin.getServer().getScheduler().cancelTask(repeatingTasks.get(gameState));
        repeatingTasks.remove(gameState);
    }

    public GameState getGameState() {
        return gameState;
    }
    public GameManagerOptions getOptions() {
        return options;
    }

    public boolean setPlayerInGame(Player player) {
        if ((gameState == GameState.STARTED || gameState == GameState.ENDED) && !options.canJoinStartedGame()) return false;
        if (options.getMaxGamePlayers() > 0 && inGamePlayers.size() >= options.getMaxGamePlayers()) return false;

        removePlayer(player);

        boolean addedToGame = inGamePlayers.add(player);
        if (addedToGame) startGameCountdown();

        return addedToGame;
    }

    public boolean setPlayerSpectating(Player player) {
        removePlayer(player);
        return spectatingPlayers.add(player);
    }

    public boolean removePlayer(Player player) {
        if (inGamePlayers.remove(player)) return true;
        if (spectatingPlayers.remove(player)) return true;
        return false;
    }

    public boolean isPlayerAlive(Player player) {
        return inGamePlayers.contains(player);
    }

    private void startReminderChatMessages() {
        if (gameState != GameState.PREGAME) return;
        if (hasRepeatingTask(GameState.PREGAME)) return;

        setRepeatingTask(GameState.PREGAME, plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (gameState != GameState.PREGAME) {
                cancelRepeatingTask(GameState.PREGAME);
                return;
            }
            if (plugin.getServer().getOnlinePlayers().size() == 0) return;
            plugin.getServer().broadcastMessage(MinigameTemplate.PREFIX_INFO + "Game starts with " + options.getMinGameStartPlayers() + " players");
        }, 100, 300));
    }

    public boolean startGameCountdown() {
        if (gameState != GameState.PREGAME) return false;
        if (inGamePlayers.size() < options.getMinGameStartPlayers()) return false;
        if (hasRepeatingTask((GameState.COUNTDOWN))) return false;

        gameState = GameState.COUNTDOWN;

        long countdownStart = Instant.now().getEpochSecond();

        cancelRepeatingTask(GameState.PREGAME);

        setRepeatingTask(GameState.COUNTDOWN, plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            // If the game has started by other means then stop this countdown
            if (gameState != GameState.COUNTDOWN) {
                cancelRepeatingTask(GameState.COUNTDOWN);
                return;
            }

            if (inGamePlayers.size() < options.getMinGameStartPlayers()) {
                cancelRepeatingTask(GameState.COUNTDOWN);
                sendTitleToAllPlayers(MinigameTemplate.PREFIX_ERROR + "Not enough players to start");

                // Revert game state
                gameState = GameState.PREGAME;
                startReminderChatMessages();
            }

            long timeRemaining = GAME_COUNTDOWN_LENGTH_SECONDS - (Instant.now().getEpochSecond() - countdownStart);

            if (timeRemaining <= 0) {
                gameState = GameState.STARTED;
                cancelRepeatingTask(GameState.COUNTDOWN);
                startGame();
            }
            if (!COUNTDOWN_TITLE_NUMBERS.containsKey((int) timeRemaining)) return;

            sendTitleToAllPlayers(COUNTDOWN_TITLE_NUMBERS.get((int) timeRemaining));
        }, 0, 20));

        return true;
    }

    public boolean startGame() {
        if (gameState == GameState.STARTED) return false;
        if (gameState == GameState.ENDED) return false;
        if (hasRepeatingTask(GameState.STARTED)) return false;

        gameState = GameState.STARTED;

        long gameStart = Instant.now().getEpochSecond();

        setRepeatingTask(GameState.STARTED, plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (gameState == GameState.ENDED) cancelRepeatingTask(GameState.STARTED);

            long gameRunningTimeSeconds = Instant.now().getEpochSecond() - gameStart;
            plugin.getServer().getPluginManager().callEvent(new GameRunningTimeEvent(gameRunningTimeSeconds));

            if (inGamePlayers.size() < options.getMinGameStartPlayers()) {
                cancelRepeatingTask(GameState.STARTED);
                endGame();
                return;
            }

            if (options.getMaxGameTimeInSeconds() <= 0) return;
            if (gameRunningTimeSeconds >= options.getMaxGameTimeInSeconds()) endGame();
        }, 0, 20));

        return true;
    }

    public boolean endGame() {
        if (gameState == GameState.ENDED) return false;
        if (hasRepeatingTask(GameState.ENDED)) return false;

        gameState = GameState.ENDED;

        long gameEnd = Instant.now().getEpochSecond();

        plugin.getServer().getPluginManager().callEvent(new GameEndEvent());

        setRepeatingTask(GameState.ENDED, plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (plugin.getServer().getOnlinePlayers().size() == 0) cancelRepeatingTask(GameState.ENDED);

            long gameEndRunningTimeSeconds = Instant.now().getEpochSecond() - gameEnd;
            if (gameEndRunningTimeSeconds >= options.getGameEndTimeInSeconds()) {
                cancelRepeatingTask(GameState.ENDED);

                plugin.getServer().getOnlinePlayers().forEach(player -> {
                    player.kickPlayer("this is where you'd be send to lobby");
                });

                plugin.getServer().shutdown();
            }

        }, 0, 20));

        return true;
    }
}
