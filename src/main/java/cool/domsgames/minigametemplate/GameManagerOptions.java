package cool.domsgames.minigametemplate;

public class GameManagerOptions {

    private final boolean joinStartedGame, autoSpectateOnDeath, autoRespawn, displayDeathMessages;
    private final int maxGameTimeSeconds, maxPlayers, minStartPlayers, gameEndTimeSeconds, autoRespawnTimerSeconds, combatTagTimeSeconds;

    public GameManagerOptions() {
        this(false, true, 0, 0, 2, 10, false, 5, 5, true);
    }

    public GameManagerOptions(boolean joinStartedGame, boolean autoSpectateOnDeath, int maxGameTimeSeconds, int maxPlayers, int minStartPlayers, int gameEndTimeSeconds, boolean autoRespawn, int autoRespawnTimerSeconds, int combatTagTimeSeconds, boolean displayDeathMessages) {
        this.joinStartedGame = joinStartedGame;
        this.autoSpectateOnDeath = autoSpectateOnDeath;
        this.maxGameTimeSeconds = maxGameTimeSeconds;
        this.maxPlayers = maxPlayers;
        this.minStartPlayers = minStartPlayers;
        this.gameEndTimeSeconds = gameEndTimeSeconds;
        this.autoRespawn = autoRespawn;
        this.autoRespawnTimerSeconds = autoRespawnTimerSeconds;
        this.combatTagTimeSeconds = combatTagTimeSeconds;
        this.displayDeathMessages = displayDeathMessages;
    }

    public boolean canJoinStartedGame() {
        return joinStartedGame;
    }

    public boolean shouldAutoSpectateOnDeath() {
        return autoSpectateOnDeath;
    }

    public boolean shouldAutoRespawn() {
        return autoRespawn;
    }

    public boolean shouldDisplayDeathMessages() {
        return displayDeathMessages;
    }

    public int getMaxGameTimeInSeconds() {
        return maxGameTimeSeconds;
    }

    public int getMaxGamePlayers() {
        return maxPlayers;
    }

    public int getMinGameStartPlayers() {
        return minStartPlayers;
    }

    public int getGameEndTimeInSeconds() {
        return gameEndTimeSeconds;
    }

    public int getAutoRespawnTimeInSeconds() {
        return autoRespawnTimerSeconds;
    }

    public int getCombatTagTimeInSeconds() {
        return combatTagTimeSeconds;
    }
}
