package cool.domsgames.minigametemplate.enums;

public enum GameMetadata {
    LIFE_START_SECONDS,
    COMBAT_LAST_TAG_MILLIS,
    COMBAT_TAGGER_UUID,
    COMBAT_TAGGER_HEALTH // Used for if the attacker logs out
}
